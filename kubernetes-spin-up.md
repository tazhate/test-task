**Create app and CI**

Just simple GO app with http server - [here](https://gitlab.com/tazhate/test-task/-/blob/main/hello-world.go).

Simple and secure and minimal [Dockerfile](https://gitlab.com/tazhate/test-task/-/blob/main/Dockerfile).

Simple CI for build and publish - [here](https://gitlab.com/tazhate/test-task/-/blob/main/.gitlab-ci.yml)

**Spin up local Kubernetes**

I used [kind](https://kind.sigs.k8s.io/) for that. 

Setup is simple:
```kind create cluster```

Then i've setup local loadbalancer solution - metallb.

```
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/metallb.yaml
```
I needed local ip for testing:

```docker network inspect -f '{{.IPAM.Config}}' kind```

and then create configmap:

```
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 172.19.255.200-172.19.255.250
```

**Spin up Letsencrypt at Kubernetes**

We will use simple https://cert-manager.io/ to get certs for our app.

Just install it with helm:

```helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.7.2 \
   --set installCRDs=true
```

Then create cluster-issuer with this yaml
```
- apiVersion: cert-manager.io/v1
  kind: ClusterIssuer
  metadata:
    name: letsencrypt-prod
  spec:
    acme:
      email: taz.inside@gmail.com
      server: https://acme-v02.api.letsencrypt.org/directory
      solvers:
      - http01:
          ingress:
            class: nginx
```

Then we need to install nginx ingress into our cluster. Nginx will automatically create service object woth loadbalancer with help from mettallb.

```
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install nginx nginx-stable/nginx-ingress --create-namespace nginx-ingress
```

that's all for setup. Now we need just to create proper ingress object which contains annotations and tls.

like that:

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: janbo
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: letsencrypt-prod
spec:
  rules:
  - host: janbo.tazhate.com
    http:
      paths:
      - backend:
          serviceName: janbo
          servicePort: 80
        path: /
        pathType: ImplementationSpecific
  tls:
  - hosts:
    - janbo.tazhate.com
    secretName: janbo.tazhate.com
```

To run our app will be also needed service and pod objects. You can find them [here](https://gitlab.com/tazhate/test-task/-/tree/main/k8s-manifests).

After apply manifests will create everything we need (tls cert and so on) - automatically! Viola!
