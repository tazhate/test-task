FROM golang:1.15.0-alpine as builder

WORKDIR /app
COPY ./ /app
RUN go build -o ./builds/linux/hello ./hello-world.go

FROM alpine:3.7

COPY --from=builder /app/builds/linux/hello /usr/bin/hello
RUN addgroup hello && adduser -D -h /hello -G hello hello
USER hello
WORKDIR /hello
ENTRYPOINT ["/usr/bin/hello"]
CMD ["hello"]
